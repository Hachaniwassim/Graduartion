import { GroupeDTO } from "./groupeDTO";

export class EntrepriseDTO{
        id!: number;
        companyname!: string;
        codefiscale!: string;
        phone!: string;
        fax!: string;
        email!: string;
        note!: string;
        groupId!: string;
        createdDate !: Date;
        lastModifiedDate!: Date; 
        
       
      

}