export interface postDTO{
    id?:number ;
	image? : string;
    tagline? : string;
    postTranslations? : string;
    title? : string;
    description ? : string;
    content ? : string;
    slug ? : string;
    createdDate :   Date;
    lastModifiedDate: Date; 
    subtitle ? : string;
}