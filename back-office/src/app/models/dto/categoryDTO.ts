export interface categoryDTO{
    id?:number ;
	image? : string;
    title? : string;
    description? : string;
    menuimage? : string;
    bannerimage ? : string;
    status ? : string;
}