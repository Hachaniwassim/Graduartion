export interface productsDTO{
    id?:number ;
	title? : string;
    detailimage? : string;
    note? : string;
    name? : string;
    image ? : string;
    consultationNumber ? : string;
}