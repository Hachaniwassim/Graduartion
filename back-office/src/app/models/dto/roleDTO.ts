import { ERole} from "../enum/erole";

export class RoleDTO{

    id?:number;
    name?: ERole;
}