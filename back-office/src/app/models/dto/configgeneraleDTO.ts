export interface configgeneraleDTO{
    id?:number ;
	facebook? : string;
    twitter? : string;
    youtube? : string;
    image? : string;
    adresse ? : string;
    email ? : string;
    phone ? : string;
    fax ? : string;
    title ? : string;
    createdDate :   Date;
    lastModifiedDate: Date; 
    newslettertitle ? : string;
    newslettersubtitle ? : string;
    tagline ? : string;
}