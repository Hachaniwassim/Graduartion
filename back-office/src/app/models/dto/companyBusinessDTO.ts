import { GroupeDTO } from "./groupeDTO";

export class CompanyBusinessDTO{
    id?:number ;
	description? : string;
    domainename? : string;
    createdDate! :   Date;
    lastModifiedDate!: Date; 
}