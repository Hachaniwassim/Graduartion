export interface pages{
    id?:number ;
	published? : boolean;
    pagetype? : string;
    title? : string;
    description? : string;
}