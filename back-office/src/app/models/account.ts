export interface account{
    id?:number ;
	username? : string;
    email? : string;
    password? : string;
    matchingPassword? : string;
    fiscaleCode ? : string;
}