package app.igesa.enumerations;

public enum GroupStatus {
    ACTIVE,
    PENDING,
    BLOCKED
}
