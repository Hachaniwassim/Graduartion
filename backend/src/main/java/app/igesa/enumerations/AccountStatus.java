package app.igesa.enumerations;

public enum AccountStatus {
    ACTIVE,
    PENDING,
    BLOCKED
}
