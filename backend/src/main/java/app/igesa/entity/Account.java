package app.igesa.entity;

import app.igesa.enumerations.AccountStatus;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity@Table(	name = "accounts",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        })
public class Account  {

    private static final long serialVersionUID = 65981149772133526L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String username;

    @Email
    private String email;
    @NotBlank
    private String password;

    @NotBlank
    private String matchingPassword;

    @NotBlank
    private String fiscaleCode ;

    private AccountStatus accountStatus;

    private Long groupId;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(	name = "account_roles",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();


 public Account( String username, String email, String password, String matchingPassword, String fiscaleCode, AccountStatus accountStatus){
     super();
     this.username=username;
     this.email=email;
     this.password=password;
     this.matchingPassword=matchingPassword;
     this.fiscaleCode=fiscaleCode;
     this.accountStatus=accountStatus;
 }


    public Account( String username, String email, String password, String matchingPassword,Set<Role> roles , String fiscaleCode){
        super();
        this.username=username;
        this.email=email;
        this.password=password;
        this.matchingPassword=matchingPassword;
        this.fiscaleCode=fiscaleCode;
        this.roles=roles;
    }



    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "groupe_id")
    private Groupe groupe ;


}
